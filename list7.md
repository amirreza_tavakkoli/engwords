## Fierce(adj.)
  angry and aggressive - -خشن- خشمگین -ترسناک
  strong قوی - درنده)(حتی در مورد افکار و احساسات و اعتقادات)
   fiercer, fiercest
     Two fierce eyes glared at them.
     She spoke in a fierce whisper.
     fierce loyalty
     He launched a fierce attack on the Democrats.
     fierce wind

### fierceness(n.)
  خشم-خشونت-قدرت
    the fierceness of a bear
    the fierceness of the flames/belifs
### fiercely(adv.)  
    a fiercely competitive market  

## Detest
  to hate  
  detest doing something  
  detest somebody/something  

### detestion(n.)
  تنفر  

### detestable
    All terrorist crime is detestable, whoever the victims.  

### Sneer(n.) 
  تمسخر  
    A faint sneer of satisfaction crossed her face  
## Sneer(v.)  
  sneer (at somebody/something)  
  to mock  
  پوزخند زدن- مسخره کردن  
    She gave a sneering laugh.  


### sneeringly(adv.)

## Srowl(v.)
 ~ glower  
  to look at somebody/something in an angry way  
  scowl (at somebody/something)  

### scowl(n.)
  an angry look or expression  
    He looked up at me with a scowl.  
## Vermin(n.)
  انگل-افت  
    On farms the fox is considered vermin and treated as such.  

### verminous(adj.)
  covered with vermin  

### receptionist(n.)
  مسعول پذیرش  
## Recipt