## Affair(n.)countable

  event, public activity جریان- داستان- امر-اتفاق-موضوع- قضییه- حادثه-موضوع  
  world/international/home(داخلی)/extramarital affairs   
  handle/ deal with affair  
  administer, arrange affairs  
  be involved in an affair  
    The Whitewater affair was the biggest scandal of the decade.  
    She organizes her financial affairs very efficiently.  
    He's always meddling in (= trying to influence) other people's affairs.  
    Many people have criticized the way the government handled the affair.  
  
  داستان عاشقی- کارزار جنسی   
    She is having an affair with her boss.  

  an object - یک چیز  
    She wore a long black velvet affair.  
     
  
a state of affairs ~ a situation  
  
affais of state~ matters that the government of a country deals with  


### affaire(n.)

  a love affair  

## Revise(v.)
-> اصلاح و ویرایش کردن- تغییر دادن  
     to change your opinions or plans,...  
    revise something  
    revise something (from something) (to something) اصلاح از قبل به بعد  
  
--> a revised edition of a textbook  
    from the original 200, that was revised to 100, only about 50 people showed up.  
-> تجدید نظر کردن- -بررسی دوباره کردن)( در مورد کتاب یا نوشته)  
  


## solitary(adj.)

->  alone تنها-منفرد-یکه  

-->  He was a solitary child.  

->  done alone  
--> She enjoys long solitary walks.  
  
---> negative form of single فقط یکی  
  
## solitary(n.)  
-> زندان انفرادی- فرد تنها  

### solitariness (n.)
  تنهایی  
    Even as a boy he was given to solitariness.  

## Glimpse(n.)

  ~ Glance تجربه - نگاه سریع و زودگذر  
  glimpse (of somebody/something)   
  glimpse at somebody/something   
  glimpse into something   
    Take a glimpse into the future of rail travel.  
    I just got a glimpse at the baby, but she was very cute.  
    He caught a glimpse of her in the crowd.  
    a fleeting)(گذرا) glimpse  

### glimpse(v.)  

  ​glimpse somebody/something   
  to see somebody/something for a moment, but not very clearly  
    He'd glimpsed her through the window as he passed.  


## Neglect (v.)
  بی توجهی کردن  

### neglected(adj.)
  بی توجهی شده به  

### neglectful 
  بی توجه  
  of something/somebody   
    She became neglectful of her appearance  

  


###### Miniority

###### Conflict

###### Compete

###### Numerous
