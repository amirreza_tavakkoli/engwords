
## Refine(v.)/rifine/   

->    ​refine something     
def->        to make a substance pure by taking other substances out of it خالص کردن یک ماده یا چیز  
                    refine your thaughts  
                    Crude oil is industrially refined to purify it and separate out the different elements, such as benzene.  
def->        to improve something بهبود دادن  
                    She has refined her playing  
  

#### refinement(n.) 
        بهبود - خالص سازی  

#### refiner(n.)
    ماده خالص سازس  

#### refinery(n.) 
    محل خالص سازی-کارخانه  




## Imply

->        to suggest/ اشاره کردن(غیرمستقیم-)- پیشنهادکردن  
  it is implied that…  
  imply something...  
  imply (that)…  
  as sth implies  
  as the name implies  
  take sth to imply  
-->  It was implied that we were at fault.  
     Are you implying (that) I'm fat?  
     Popularity does not necessarily imply merit(eligibiity شایستگی).  
     This statement should not be taken to imply that the government is exonerated of all blame  
->    to make something necessary (نیازمند کردن-بودن)دلالت کردن  
-->   The project implies an enormous investment in training.  
      Socialism implies equality.

#### implication(n.)
  اشاره غیر مستقیم    
  by implication
  clear, obvious

### imply vs infer
/Infer and imply have opposite meanings. The two words can describe the same event, but from different points of view. If a speaker or writer implies something, they suggest it without saying it directly:  
The article implied that the pilot was responsible for the accident.  
If you infer something from what a speaker or writer says, you come to the conclusion that this is what he or she means:  
I inferred from the article that the pilot was responsible for the accident./  

## Assert
->     to say that something is certainly true: اظهار نظر قطعی کردن - ادعای قطعی کردن  
             assert something  
             assert that          
                 He asserts that she stole money from him  
                 The companies have asserted that everything they did was appropriate.  
                 The book asserts that the 1969 moon landing was a hoax.  
  
->      behave in a way to show that you have power and confidence and control:  
                    اثبات قدرتمند بودن کردن  
                    اثبات کنترل داشتن کردن  
                    قدرتمند ظاهر شدن  
                   assert yourself  
                   I really must assert myself more in meetings.  
                   She very quickly asserted her authority over the class.  
#### assertion(n.)
#### assertive(adj.)

## Invocation(n. of invoke)/invekeision/  
->  استناد  
    invokation to  
    They disagreed with the administration's invocation   
  
->   التماس\ معمولا از خدا  
     مراسم مذهبی  التماس از خدا  
    As an outspoken(رک و راست و قدرتمند) campus atheist, she didn't want to sit through a Christian invocation when she graduated.  

#### invoke(v.) to sth  

        they invoked to part53 of lawbook  
        Police can invoke the law to regulate access to these places.  

## Discern(v.)regular/disern/

def->    to detect and recognize or understand something, especially something that is not obvious and is far away    
{به سختی}  
{صدا - تصویر از دور}فهمیدن- درک کردن- تشخیص دادن  
      
     discern something:  
            We could just discern the house in the distance.  
            He could discern the note of urgency in their voices.  
     discern how, whether:  
             It is often difficult/hard/impossible to discern how widespread public support is.  
     discern that…:  
             I quickly discerned that something was wrong.  

#### discernible(adj.)     
->          able to be seen, recognized, or understood:  
  
+ly=adv  

