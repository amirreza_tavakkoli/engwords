## pauper/paper/(n)
  very poor person  

## Paunch(n.)
  fat belly-شکم  
  The driver had a paunch hanging over his belt.  

### paunchy(adj)
  شکم دار(در مورد افراد)  

## calamity(n.)
  countable
  disaster, miseary بدبختی - مصیبت - فاجعه  
    The country suffered a series of calamities during the 1980s.  

### calamitous (adj.)
  causing great damage to people’s lives, property, etc.   
  مصیبت بار  
    The bridge collapsed in the storm, with calamitous results.  


## fugitive(n.)/fujetive/
  a person who has escaped or is running away from somewher  
  فراری- تبعیدی- فرد در حال فرار- a runaway  
    one of the most wanted fugitives sought by the Italian police   

### fugitive(adj.)
  فراری- در مورد انسان   
  گذرا- fleeting   
    fugitive idea/thought  


## squander(v.)
  to waste large amounts of money or other resources  
  squander sth on sth هدر دادن- تلف کردن  
    He squandered all his money on gambling.  
    We have squandered the opportunity to become a major player in the industry"  

## Baggage
  Bags are individual pieces of baggage:    
  luggage- burden-bags- بارها - -چمدان ها -کیف شامل بارها- تجارب و بار احساسی  
  اعتقادها  
    baggage handlers (= people employed to load and unload baggage at airports)  
    excess baggage ~ bags, cases, etc. that weigh more than the weight you are allowed to take onto a plane, or the extra money you have to pay for it to be taken:  
     baggage car  
     baggage allowance  
     baggage drop ~ محل گذاشتن بارها  

     baggage handler ~ a person who takes passengers' bags and cases and puts them onto or removes them from an aircraft  
     We loaded our baggage into the car.  
     How many pieces of baggage do you have?  
     bag and baggage ~ with all your possessions  
     She was carrying a lot of emotional baggages  
      carry-on/hand baggage ~ cases or bags that you take onto a plane with you and are not checked in  


### Carriage(n.)
  کالسکه  

## matrimonial(adj.)
  connected with marriage  
  مربوط به ازدواج  
    matrimonial problems  

### matrimony(n.)
  ازدواج-عروسی  

## Testimony(n.)
~ testament   
  شهادت  
  Her claim was supported by the testimony of several witnesses.  
  تصدیق  
  testimony (to something)
    This increase in exports bears testimony to the successes of industry.  
    The pyramids are an eloquent testimony to the ancient Egyptians' engineering skills.  
  
### testimonial(n.)
  رضایت نامه  
  The catalogue is full of testimonials from satisfied customers.  
  
## Bigamy(n.)
  having more than one wife or husband(crime)  
  چندهمسری  


### bigamous(adj.)
    a bigamous relationship  
### bigamist(n.)  
  ​a person who commits the crime of  

## Prosecute(v.)
  prosecute somebody/something for (doing) somethings  
  تحت پیگرد قانونی در اوردن  
  تحت پیگرد قانون بودن   
    Trespassers will be prosecuted   
    The company was prosecuted for breaching the Health and Safety Act.   


  the prosecuting counsel/lawyer/attorney ~ فرد یا وکیل فرد پیگیر  

### prosecution(n.)

### prosecutor
  a lawyer who leads the case against a defendant(the person in a trial who is accused of     a crime) in court  
  a public official who charges somebody officially with a crime and prosecutes them in   

### solvency
 نقدشومدگی-حل شوندکی    