## Defraud(v.)
  to take money or goods illegally from someone by deceiving them-to decive فریب دادن  
  defraud sb of sth  
  conspiracy to defraud  
    They are both charged with conspiracy to defraud an insurance company of $20,000.  
    She is charged with defrauding the Internal Revenue Service.  

## Defect(n.)
  flaw نقص-عیب-مشکل  
  defect in sth  
  correct/remedy a defect  
  birth/genetics/design/major/serious defect  

### Defect(v.)
  to leave a political party, country, etc. to join another that is considered to be an enemy خیانت کردن  
  defect (from something) (to something)  
    She defected from the party just days before the election.  

#### Defector(n.)
  فرد خیانت کار  

#### Defection (n.)
  from a to b  
  خیانت  

#### Defective (adj.)
  ناقص=مشکل دار- خراب  
  containing a fault, or not working correctly:  
      a defective gene  
+ly=adv  

#### defectiveness (n.)

  خراب بودن  


##### Desert vs Defect
  Desert ~ let go, Defect ~ let go and join the enemy(کشور-گروه-دسته)  


## Molest(v.)
  molest somebody  
  to attack somebody sexually  
  تعرض کردن جنسی(انگشت کردن-انگل دادن)  
    The man had previously been arrested several times for molesting young boys.  


  حمله کردن  


#### molestation(n.)
  انگل- تجاوز  

#### moloster(n.)
  انگل دهنده  

## denounce(v.)
  انتقاد علنی کردن(نظر بد علنی دادن)  
  to criticize something or someone strongly and publicly:  
  denounce somebody/something as something  
  denounce somebody/something to something  
    The project was denounced as a scandalous(ننگین) waste of public money.  
    The teachers denounced the contract offer as inadequate.  
  
  to tell the police, the authorities, etc. about somebody’s illegal political activities.  
  to accuse someone publicly   
  متهم کردن  
    They were denounced as spies(جاسوس).  
    Many people denounced their neighbours to the secret police.  
    
