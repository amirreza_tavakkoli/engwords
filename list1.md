## Desecration(n.)/desikration/: 
def->    treating holy things without respect  
   
->    the desecration of a cemetery(graveyard).  
بی حرمتی
#### Desecrate(v.)regular:
->    It's a crime to desecrate the country's flag.  
->    Vandals(bad people, خرابکارها) desecrated the monument(historicial places
      like tomb or statue(بنای تاریخی و مقدس)).  
بی حرمتی کردن  

## Statue(n.)/staetu/:
->    مجسمه. نماد  
->    the statue of liberty  
->    They planned to put up/erect(build) a statue to the president.     

## homage(n.)/homage/singular:
def->     an expressesion(mark(نشان)) of respect and honor to sb/sth  
->       ادای احترام ,احترام   
->       تجلیل کردن   
->      pay homage to sb/sth   
->      doing homage   
->      They stood in silent homage(احترام مسکوت) around the grave.   

## Vail(v.)/veil/regular:
def->    to take off your hat or other head covering as a mark of respect to someone  
->       to show respect  
->       کلاه از سر برداشتن به نشانه احترام   
->   

## Royalty(n.)//royalties:  
def 1->      members of a royal family   فرد سلطنتی   
->           Hollywood royalty (= very famous film stars  
->           We were treated like royalty.  

def 2->      sum of money that is paid to somebody who has written a book, piece of music, etc  
->           حق امتیاز  / مجموع پول دریافتی برای کاری  
->           She received £2 000 in royalties  
->           All royalties from the album will go to charity  
->           a royalty payment  