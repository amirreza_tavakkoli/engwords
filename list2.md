## canny(adj)  
d->  pleasant, intelligant, celever (about people)  
->   He was canny enough to see that they were trying to trap him.  
->   cannily, uncanny  

## savvy  
(n.)-> practical knowledge  - دانایی-  
مهارت عملی  

->     Skill isn’t enough—you need savvy too  
->       She's very intelligent, but hasn't got much savvy.  

(adj.) -> having practical knowledge -دانا     -  
با مهارت عملی  
  
->      savvier, savviest  
->       He’s a politically savvy guy.  
->        get/become savvy  
->      savvy investors/businessmen/consumers  
->

->  media-savvy : having a good understanding of the influence of the internet  
->    These people are media-savvy and they are not going to say anything on camera that makes them look stupid.  

## amplify/ampilifai/(n.)
def1-> -محکم کردن-قدرتمند کردن-تقویت کردن to strentghen sth  
->          amplify something  

->         A funeral can amplify the feelings of regret and loss for the relatives  


  
def2-> to add detail to sth -افزودن- -بزرگ کردن-مفصل کردن  
->            amplify something  
->                 You may need to amplify this point.  
->            
amplification(n.)  
amplifier(adj.)  

## assess(v.)regular

def->    to judge or decide the amount, value, quality, or importance of something  
(-  وضعیت-  ارزش-  قیمت) تعیین و ارزیابی کردن  
assess somebody/something  
-> A college is going to assess a student’s ability based on grades.  

assess somebody/something as something  
-> you were assessed as guilty  

assess whether  
->   The committee assesses whether a building is worth preserving.  

assess sb (based) on sth  
->    You will be assessed on your ability to take decisions quickly.  

assess sth at sth (تخمین زدن)  
->   The cost of repairs to the equipment was assessed at £2,000  

assessment(n.) تخمین-براورد-ارزیابی-تعیین-  
->   This is an interesting assessment of Mark Twain’s(a writer) importance in American   literature.  

assesse ~ a person or group that is being assessed  
assessor  
assessable(adj.)  